from setuptools import setup

setup(name='tauleapevo',
      version='0.02',
      description='',
      url='https://bitbucket.org/bfmaier/tauleaping',
      author='Benjamin Maier',
      author_email='bfmaier@physik.hu-berlin.de',
      license='MIT',
      packages=['tauleapevo'],
      install_requires=[
          'numpy',
          'scipy',
      ],
      zip_safe=False)
