from numpy import *
import sys
from scipy import sparse as sprs

class SSA:

    def __init__(self,N_sp,f,Nmax,N0=None,abort_at_fixation=False):

        self.N_sp = int(N_sp)
        self.Nmax = int(Nmax)

        try:
            self.f = array(f,dtype=float).reshape((N_sp,))
        except:
            print("fitness vector does not have the right shape")
            sys.exit(1)

        if N0 is None:
            self.N0 = array(floor(ones((self.N_sp,))*self.Nmax / float(self.N_sp)),dtype=int)
            #while sum(self.N0)>self.Nmax:
            #    sp = random.randint(self.N_sp)
            #    self.N0[sp] -= 1
            #while sum(self.N0)<self.Nmax:
            #    sp = random.randint(self.N_sp)
            #    self.N0[sp] += 1
            print self.N0
        else:
            try:
                self.N0 = array(N0,dtype=int).reshape((N_sp,))
            except:
                print("N0 vector does not have the right shape")
                sys.exit(1)


        self.current_n = self.N0
        self.n = [ self.current_n.tolist() ]
        self.t = [ 0. ]
        self.ones_sp = ones((self.N_sp,))

        #take care of events
        self.N_events = 2*self.N_sp

        #state vectors
        #self.v = sprs.lil_matrix((self.N_sp,self.N_events),dtype=int)
        self.v = zeros((self.N_sp,self.N_events),dtype=int)
        for sp in range(self.N_sp):
            self.v[sp,sp] = +1   #birth event
            self.v[sp,self.N_sp+sp] = -1 #death event            
        self.v2 = self.v * self.v
        #self.v = self.v.to_csr()

        #reaction rates
        self.R = zeros((self.N_events,))
        self.reactant_species = nonzero(self.N0)
        self.update_death_rate()
        self.update_event_rates()


    def update_death_rate(self):
        self.death_rate = self.f[self.reactant_species].dot(self.current_n[self.reactant_species]) / float(self.Nmax)
        #self.death_rate = self.f[self.reactant_species].dot(self.current_n[self.reactant_species]) / sum(self.current_n[self.reactant_species])

    def update_event_rates(self):
        self.R[self.reactant_species] = self.f[self.reactant_species] * self.current_n[self.reactant_species]
        self.R[self.reactant_species+self.N_sp] = self.death_rate * self.current_n[self.reactant_species]

    def get_R_0(self):
        return sum(self.R[self.reactant_species]) + sum(self.R[self.reactant_species+self.N_sp])

    def get_taus_till_now(self):
        t_ = array(self.t)
        return t_[1:] - t_[:-1]

    def step(self):
        self.update_death_rate()
        self.update_event_rates()

        R_0 = self.get_R_0()
        tau = random.exponential(1./R_0)
        reactant_rates = self.reactant_species.tolist() + (self.reactant_species+self.N_sp).tolist()
        rates = self.R[ reactant_rates ]
        j = random.choice(reactant_rates,p=rates/R_0)
        self.current_n += self.v[:,j]
        self.n.append(self.current_n.tolist())
        self.t.append(self.t[-1]+tau)
        if j>=self.N_sp :
            if self.current_n[j%self.N_sp]==0:
                return j%self.N_sp

        return None

    def get_t(self):
        return array(self.t)

    def get_n(self):
        return array(self.n)

    def simulate_till_next_extinction(self):
        while True:
            died = self.step()
            if died is not None:
                new_rs = set(self.reactant_species.tolist())
                new_rs.remove(died)
                self.reactant_species = array(list(new_rs))
                #print self.reactant_species
                #print "species",died,"died"
                break

    def simulate(self):

        while len(self.reactant_species)>1:
            self.simulate_till_next_extinction()

    def simulate_till_t(self,tmax):
        while self.t[-1]<tmax:
            self.step()
            ndx = nonzero(self.current_n<=0)[0]
            if len(ndx)>0:
                self.current_n[ndx] = 0
                self.n[-1][ndx] = 0
                print len(ndx),"species died"
                break

if __name__=="__main__":
    import pylab as pl
    import seaborn as sns

    Nsp = 10
    Nmax = 100
    time_unit = '$T$'
    f = ones(Nsp) #offspring per cell per $time_unit

    tmax = 200
    sim = SSA(Nsp,f,Nmax)


    sim.simulate()
    #for i in range(tmax):
    #    sim.step()
    t = sim.get_t()
    n = sim.get_n()
    taus = sim.get_taus_till_now()
    fig,ax = pl.subplots(1,2)


    for sp in range(Nsp):
        ax[0].plot(t,n[:,sp],'-')
    ax[0].plot(t,n.sum(axis=1),'-',lw=2)
    ax[0].set_xlabel(r'$t/$'+time_unit)
    ax[1].set_xlabel(r'$\tau/$'+time_unit)
    ax[1].hist(taus,normed=True)

    pl.show()

    
