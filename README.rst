Tau Leaping Evolution
=====================

Description
-----------

Implementation of the tau-leaping method for different evolutionary contexts

Example
-------

To use, do for example::

    >>> from tauleapevo import traditional
    >>> N_species = 2 
    >>> f_species = [ 1, 1 ] #offspring per individuum per time unit
    >>> N_max = 100
    >>> sim = traditional(N_species,f_species,N_max)
    >>> sim.simulate_till_next_extinction()
