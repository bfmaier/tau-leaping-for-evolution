from numpy import *
import sys
from scipy import sparse as sprs

class traditional:

    def __init__(self,N_sp,f,Nmax,N0=None,abort_at_fixation=False,eps=0.03,n_c=10,SSA_state_max=100):

        self.N_sp = int(N_sp)
        self.Nmax = int(Nmax)
        self.eps = 0.03
        self.g = 2
        self.n_c = n_c
        self.SSA_state_max = SSA_state_max
        self.SSA_state = self.SSA_state_max

        try:
            self.f = array(f,dtype=float).reshape((N_sp,))
        except:
            print("fitness vector does not have the right shape")
            sys.exit(1)

        if N0 is None:
            self.N0 = array(floor(ones((self.N_sp,))*self.Nmax / float(self.N_sp)),dtype=int)
            #while sum(self.N0)>self.Nmax:
            #    sp = random.randint(self.N_sp)
            #    self.N0[sp] -= 1
            #while sum(self.N0)<self.Nmax:
            #    sp = random.randint(self.N_sp)
            #    self.N0[sp] += 1
            print self.N0
        else:
            try:
                self.N0 = array(N0,dtype=int).reshape((N_sp,))
            except:
                print("N0 vector does not have the right shape")
                sys.exit(1)


        self.current_n = self.N0
        self.n = [ self.current_n.tolist() ]
        self.t = [ 0. ]
        self.ones_sp = ones((self.N_sp,))
        self.participating_species = arange(N_sp)

        #take care of events
        self.N_events = 2*self.N_sp

        #state vectors
        #self.v = sprs.lil_matrix((self.N_sp,self.N_events),dtype=int)
        self.v = zeros((self.N_sp,self.N_events),dtype=int)
        for sp in range(self.N_sp):
            self.v[sp,sp] = +1   #birth event
            self.v[sp,self.N_sp+sp] = -1 #death event            
        self.v2 = self.v * self.v
        #self.v = self.v.to_csr()

        #reaction rates
        self.R = zeros((self.N_events,))
        self.reactant_species = arange(N_sp)
        self.update_death_rate()
        self.update_event_rates()


    def update_non_critical_reactions(self):
        """
        Returns the indices of species whose reactions are not critical (not endangered species).
        usually one would perform 
        L_j = min_{i\in[1,N],v_ij<0} [ n_i/|v_ij| ],
        but all v_ij<0 are death events, so |v_ij| = 1 and |{i\in[1,N],v_ij<0}| = 1, so
        only death events of species i can be critical and thus
        L_{death of i} = n_i
        """
        birth_events = self.reactant_species.tolist()
        death_events = [ i+self.N_sp for i in self.reactant_species if self.current_n[i]>=self.n_c ]

        self.J_ncr = array(birth_events + death_events)
        self.I_ncr = array(death_events) - self.N_sp
        self.I_cr = array([ i for i in self.reactant_species if self.current_n[i]<self.n_c ])
        self.J_cr = array(self.I_cr) + self.N_sp

    def update_death_rate(self):
        self.death_rate = self.f[self.reactant_species].dot(self.current_n[self.reactant_species]) / float(self.Nmax)
        #self.death_rate = self.f[self.reactant_species].dot(self.current_n[self.reactant_species]) / sum(self.current_n[self.reactant_species])

    def update_event_rates(self):
        self.R[self.reactant_species] = self.f[self.reactant_species] * self.current_n[self.reactant_species]
        self.R[self.reactant_species+self.N_sp] = self.death_rate * self.current_n[self.reactant_species]

    def get_highest_order_rates(self):
        #return maximum(self.R[:self.N_sp],self.R[self.N_sp:])
        #Since death events are second order equations and every
        #death requires only one molecule, g_i is given as 2 for every species
        return 2.

    def get_tau_prime(self):
        if len(self.J_ncr)>0:
            mu = self.v[self.I_ncr][:,self.J_ncr].dot(self.R[self.J_ncr])
            sig2 = self.v2[self.I_ncr][:,self.J_ncr].dot(self.R[self.J_ncr])
            g = self.get_highest_order_rates()
            eps_N_over_g_max = maximum(self.eps/g*self.current_n[self.I_ncr],
                                       self.ones_sp[self.I_ncr])
            mu_vals = eps_N_over_g_max / abs(mu)
            sig_vals = eps_N_over_g_max**2 / sig2
            tau = min(concatenate((mu_vals,sig_vals)))

            return tau
        else:
            return float('inf')

    def get_R_0(self):
        return sum(self.R[self.reactant_species]) + sum(self.R[self.reactant_species+self.N_sp])

    def get_taus_till_now(self):
        t_ = array(self.t)
        return t_[1:] - t_[:-1]

    def step(self):
        self.update_non_critical_reactions()
        self.update_death_rate()
        self.update_event_rates()

        #if there are no critical reactions, abandon the SSA state
        #if len(self.I_cr)==0:
        #    self.SSA_state = self.SSA_state_max

        #if the algorithm is currently in SSA state, just perform an SSA step
        if self.SSA_state<self.SSA_state_max:
            #perform SSA step
            R_0 = self.get_R_0()
            tau = random.exponential(1./R_0)
            reactant_rates = self.reactant_species.tolist() + (self.reactant_species+self.N_sp).tolist()
            rates = self.R[ reactant_rates ]
            j = random.choice(reactant_rates,p=rates/R_0)
            self.current_n += self.v[:,j]
            #print self.v[:,j]
            self.n.append(self.current_n.tolist())
            self.t.append(self.t[-1]+tau)
            self.SSA_state += 1
        else:
            tau_prime = self.get_tau_prime()
            #print "10/self.get_R_0() =",10/self.get_R_0()
            not_found_a_solution = True

            while not_found_a_solution:

                #Check if tau_prime is too small, if it is, just perform standard SSA steps from now on
                if tau_prime < 10./self.get_R_0():
                    self.SSA_state = 0
                    not_found_a_solution=False
                else:
                    #Check if there are critical reactions
                    if len(self.I_cr>0):
                        #if there are, compute a second tau estimation from 
                        #all critical reaction rates
                        R_0_cr = sum(self.R[self.J_cr])
                        tau_primeprime = random.exponential(1./R_0_cr)
                    else:
                        tau_primeprime = float('inf') #make it bigger, s.t. no critical event will happen 

                    
                    print "tau' =", tau_prime
                    print tau_primeprime
                    #prepare numbers of reactions
                    K = zeros((self.N_events,),dtype=int)

                    #if there's a critical event happening
                    if tau_prime >= tau_primeprime:
                        #determine which critical event will happen
                        j_c = numpy.random.choice(self.J_cr,p = self.R[self.J_cr]/R_0_cr)
                        K[j_c] = 1
                        #set leap time to the second guess
                        tau = tau_primeprime
                    else:
                        tau = tau_prime

                    #leap noncritical
                    K[self.J_ncr] = random.poisson(lam=self.R[self.J_ncr]*tau, size=(len(self.J_ncr),)).reshape((len(self.J_ncr),))

                    new_n = self.current_n + self.v.dot(K)

                    #if any negative value occured, do it again with half tau'
                    if any(new_n<0):
                        tau_prime = tau_prime/2.
                        not_found_a_solution = True
                    else:
                        print "successful tau leap"
                        self.current_n += new_n
                        self.n.append(self.current_n.tolist())
                        self.t.append(self.t[-1]+tau)
                        not_found_a_solution = False

    def get_t(self):
        return array(self.t)

    def get_n(self):
        return array(self.n)

    def simulate_till_next_extinction(self):
        while True:
            self.step()
            ndx = nonzero(self.current_n<=0)[0]
            if len(ndx)>0:
                self.current_n[ndx] = 0
                self.n[-1][ndx] = 0
                self.reactant_species = array(set(self.reactant_species) - set(ndx))
                break

    def simulate_till_t(self,tmax):
        while self.t[-1]<tmax:
            self.step()
            ndx = nonzero(self.current_n<=0)[0]
            if len(ndx)>0:
                self.current_n[ndx] = 0
                self.n[-1][ndx] = 0
                print len(ndx),"species died"
                break

if __name__=="__main__":
    import pylab as pl
    import seaborn as sns

    Nsp = 2
    Nmax = 200
    time_unit = 's'
    f = [1.,1.] #offspring per cell per $time_unit

    tmax = 200
    sim = traditional(Nsp,f,Nmax)


    sim.simulate_till_next_extinction()
    #for i in range(tmax):
    #    sim.step()
    t = sim.get_t()
    n = sim.get_n()
    taus = sim.get_taus_till_now()
    fig,ax = pl.subplots(1,2)


    ax[0].plot(t,n[:,0],'-')
    ax[0].plot(t,n[:,1],'-')
    ax[0].plot(t,n.sum(axis=1),'-',lw=2)
    ax[0].set_xlabel(r'$t$ ['+time_unit+']')
    ax[1].set_xlabel(r'$\tau$ ['+time_unit+']')
    ax[1].hist(taus,normed=True)

    pl.show()

    
