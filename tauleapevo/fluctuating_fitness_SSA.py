from numpy import *
import sys
from scipy import sparse as sprs
from itertools import chain
from scipy.stats import norm
from scipy.integrate import cumtrapz
import matplotlib.mlab as mlab

class fluctuating_fitness_SSA:

    def __init__(self,correlation_matrix,y0,alpha,Nmax,x0=None,abort_at_fixation=False,norm_correlation_matrix=True,method="exact",N_dtau=100,factor_X=1.,factor_B=2.,save_XY=False,pl=None,show_p_of_tau=lambda tau: tau>0.25,use_mean_for_correlated_species=False,use_mean_matrix=None):


        #determine number of species
        self.N_sp = correlation_matrix.shape[0]

        #determine number of Wiener processes
        self.N_Wi = correlation_matrix.shape[1]
            
        #determine original species (uncorrelated) and correlated species
        #r,c = correlation_matrix.nonzero()
        #diag = where(r==c)[0]
        #self.orig_species = sort(r[diag])
        #self.corr_species = sort(list(set(range(self.N_sp)) - set(r[diag].tolist())))
        #print self.orig_species
        #print self.corr_species

        #check if the correlation matrix is structured properly
        #for csp in self.corr_species:
        #    for source in correlation_matrix[csp]:
        #        if source in self.corr_species:
        #            print("correlation between correlated species is currently not allowed")
        #            sys.exit(1)

        #save copy of correlation matrix
        self.C = correlation_matrix.copy()

        #if this option is activated, use dx_3 = 0.5*(x1+x2) dt + (dB_1 + dB_2)
        if use_mean_for_correlated_species:
            if use_mean_matrix is None:
                self.mean_matrix = correlation_matrix.copy()
                for sp in range(self.N_sp):
                    self.mean_matrix[sp,:] = self.mean_matrix[sp,:] / sum(self.mean_matrix[sp,:])

                #add missing columns
                for i in range(self.N_Wi,self.N_sp):
                    self.mean_matrix = hstack((self.mean_matrix),zeros((N_sp,1)))
            else:
                self.mean_matrix = use_mean_matrix.copy()
        else:
            self.mean_matrix = eye(self.N_sp)

        #norm the copy
        if norm_correlation_matrix:
            for sp in xrange(self.N_sp):
                norm = linalg.norm(self.C[sp])
                self.C[sp] /= norm
    
        #initialize random number vector for Brownian motions (Wiener processes),
        #Brownian motion and fitnesses
        self.X = zeros(self.N_sp)
        self.factor_X = factor_X
        self.factor_B = factor_B
        self.y0 = float(y0)
        self.alpha = float(alpha)
        self.Y = y0*ones(self.N_sp)

        #take care of population size and initial distribution
        self.Nmax = int(Nmax)
        
        if x0 is None:
            self.N0 = array(floor(ones((self.N_sp,))*self.Nmax / float(self.N_sp)),dtype=int)
        else:
            try:
                self.N0 = array(x0*self.Nmax,dtype=int).reshape((self.N_sp,))
            except:
                print("N0 vector does not have the right shape")
                sys.exit(1)

        self.current_N = self.N0
        self.N = [ self.current_N.tolist() ]
        self.t = [ 0. ]
        self.ones_sp = ones((self.N_sp,))

        #take care of events
        self.N_events = 2*self.N_sp

        #state vectors
        #self.v = sprs.lil_matrix((self.N_sp,self.N_events),dtype=int)
        self.v = zeros((self.N_sp,self.N_events),dtype=int)
        for sp in range(self.N_sp):
            self.v[sp,sp] = +1   #birth event
            self.v[sp,self.N_sp+sp] = -1 #death event            
        self.v2 = self.v * self.v
        #self.v = self.v.to_csr()

        #reaction rates
        self.R = zeros((self.N_events,))
        self.reactant_species = nonzero(self.N0)[0]

        #determine spacing for sampling of tau
        self.N_dtau = N_dtau

        #fix tau choosing method
        #can be 
        #   "simple" for approximation of an homogeneous poisson process
        #   "approx" for approximation of an inhomogeneous poisson process with continuous time (SDE evolution by Gillespie OU formulas)
        #   "exact"  for an inhomogeneous poisson process where rates are integrated SDEs
        self.SIMPLE = 0
        self.APPROX = 1
        self.EXACT = 2
        if method == "simple":
            self.method = self.SIMPLE
        elif method == "approx":
            self.method = self.APPROX
        elif method == "exact":
            self.method = self.EXACT
        else:
            print "method not known"
            sys.exit(1)

        #stuff to save the OU and fitness landscape data
        self.save_XY = save_XY
        if save_XY:
            self.Y_by_time = [ self.Y ]
            self.X_by_time = [ self.X ]

        #stuff to show the distribution p(tau) for certain steps
        self.pl = pl
        self.show_p_of_tau = show_p_of_tau

        self.extinction_times = []
        self.extinction_series = []

    def get_rand(self,ntau=1):
        size_W = [self.N_Wi]

        if ntau>1:
            size_W.append(ntau)

        #draw random numbers for the uncorrelated species.
        #calculate the random numbers of the Wiener processes as the 
        #dot product of the already obtained values and the correlation matrix 
        W = norm.rvs(size=size_W)
        r = self.C.dot(W) 
        
        return r

    def get_X_Y(self,tau):
        #Gillespie formulas for exact numerical Ornstein-Uhlenbeck process 
        mu = exp(-tau*self.factor_X)
        sig = self.factor_B/self.factor_X*0.5*sqrt(1.-mu**2)

        r = self.get_rand()

        #update X and Y and return
        new_X = self.mean_matrix.dot(self.X)*mu + sig*r
        new_Y = self.y0 * exp(self.alpha*self.X)

        return new_X, new_Y


    def get_death_rate(self,f):
        death_rate = f[self.reactant_species].dot(self.current_N[self.reactant_species]) / float(self.Nmax)
        return death_rate

    def get_event_rates(self,f,death_rate):
        R = zeros(self.N_events)
        R[self.reactant_species] = f[self.reactant_species] * self.current_N[self.reactant_species]
        R[self.reactant_species+self.N_sp] = death_rate * self.current_N[self.reactant_species]

        return R

    def get_R_0(self,R):
        return sum(R[self.reactant_species]) + sum(R[self.reactant_species+self.N_sp])

    def get_X_Y_taus_approx(self,taus):
        #Gillespie formulas for exact numerical Ornstein-Uhlenbeck process with tau as dt
        #Don't use this! it's incorrect for strong noise influence
        mu = exp(-taus*self.factor_X)
        sig = sqrt(self.factor_B/self.factor_X*0.5*(1.-mu**2))

        r = self.get_rand()

        new_X = self.X[:,None]*mu[None,:] + sig[None,:]*r[:,None]
        new_Y = self.y0 * exp(self.alpha*new_X)

        return new_X, new_Y

    def get_X_Y_taus_exact(self,taus):

        #get factors for Gillespie update formulas
        dtau = taus[1]-taus[0]
        mu = exp(-dtau*self.factor_X)
        sig = sqrt(self.factor_B/self.factor_X*0.5*(1.-mu**2))

        #draw correlated random numbers
        r = self.get_rand(ntau=len(taus)-1)

        #initialize OU process
        new_X = zeros((self.N_sp,len(taus)))
        new_X[:,0] = self.X

        #integrate SDE numerically
        for i in xrange(1,len(taus)):
            new_X[:,i] = self.mean_matrix.dot(new_X[:,i-1])*mu + sig*r[:,i-1]

        #compute fitness
        new_Y = self.y0 * exp(self.alpha*new_X)

        return new_X, new_Y


    def get_death_rate_taus(self,f):
        death_rate = f[self.reactant_species].T.dot(self.current_N[self.reactant_species]) / float(self.Nmax)
        return death_rate.flatten()

    def get_event_rates_taus(self,f,death_rate):
        R = zeros((self.N_events,f.shape[1]))
        R[self.reactant_species,:] = f[self.reactant_species,:] * self.current_N[self.reactant_species,None]
        R[self.reactant_species+self.N_sp,:] = death_rate[None,:] * self.current_N[self.reactant_species,None]

        return R

    def get_taus_till_now(self):
        t_ = array(self.t)
        return t_[1:] - t_[:-1]

    def get_R_0_taus(self,R):
        return sum(R[self.reactant_species],axis=0) + sum(R[self.reactant_species+self.N_sp],axis=0)

    def get_Rj_and_Lambdaj(self,taus):

        if self.method==self.APPROX:
            X,Y = self.get_X_Y_taus_approx(taus)
        elif self.method==self.EXACT:
            X,Y = self.get_X_Y_taus_exact(taus)

        dr = self.get_death_rate_taus(Y)
        R = self.get_event_rates_taus(Y,dr)
        R0 = self.get_R_0_taus(R)

        int_R = cumtrapz(R,taus,initial=0,axis=-1)

        return R,int_R,X,Y

    def step(self):

        #get the current rates
        dr = self.get_death_rate(self.Y)
        R = self.get_event_rates(self.Y,dr)
        R_0 = self.get_R_0(R)

        #draw a time leap tau estimation according to the current rates (as if homogeneous Poisson process)
        meantau = 1./R_0

        #get reactant rates
        reactant_rates = self.reactant_species.tolist() + (self.reactant_species+self.N_sp).tolist()


        if self.method==self.SIMPLE:
            #treat process as homogeneous Poisson process
            tau = random.exponential(meantau)

            #update random numbers
            X,Y = self.get_X_Y(tau)
            rates = R[ reactant_rates ]
            j = random.choice(reactant_rates,p=rates/R_0)
        else:
            dtau = meantau/self.N_dtau

            #make the integration range three times the estimated mean
            taus = arange(10*self.N_dtau+1)*dtau

            #integrate the rates in the given range (method is specified in get_Rj_and_Lambdaj)
            R,Lambda,X,Y = self.get_Rj_and_Lambdaj(taus)

            #choose tau from the pdf 

            #first method does this from the analytical p(tau), but this may not be a good idea (because then, tau=0 can get chosen)
            """
            p = sum(R,axis=0)*exp(-sum(Lambda,axis=0))*dtau
            P = 1 - exp(-sum(Lambda,axis=0))
            p /= sum(p)
            itau = random.choice(arange(len(taus)),p=p)
            tau = taus[itau]
            """

            #second method does this from differences in cdf P(tau)
            P = 1 - exp(-sum(Lambda,axis=0))
            diffP = P[1:] - P[:-1]
            diffP /= sum(diffP)
            p = diffP
            itau = random.choice(arange(1,len(taus)),p=diffP)
            tau = taus[itau]

            if (self.pl is not None) and self.show_p_of_tau(tau):
                self.pl.plot((taus[1:]+taus[:-1])*0.5,p)
                #pl.plot(taus,P)
                self.pl.plot((taus[1:]+taus[:-1])*0.5,(P[1:]-P[:-1]))
                self.pl.plot(taus,1./meantau*exp(-taus/meantau)*dtau)
                self.pl.show()
                #print sum(p)

            #get Lambda at this tau
            Lambda = Lambda[reactant_rates,itau]
            Lambda_0 = sum(Lambda)

            #from Lambda choose an event to occur
            j = random.choice(reactant_rates,p=Lambda/Lambda_0)

            #set new X and Y values
            X = X[:,itau]
            Y = Y[:,itau]


        #update population vector with current event
        self.current_N += self.v[:,j]

        #save progress
        self.N.append(self.current_N.tolist())
        self.t.append(self.t[-1]+tau)
        
        if self.save_XY:
            self.Y_by_time.append(Y)
            self.X_by_time.append(X)

        #update new fitnesses
        self.X = X
        self.Y = Y

        #in case a death event was triggered and the death caused an extinction
        if j>=self.N_sp and self.current_N[j%self.N_sp]==0:
            return j%self.N_sp
        else:
            return None

    def get_t(self):
        return array(self.t)

    def get_N(self):
        return array(self.N)

    def get_XY(self):
        return array(self.X_by_time), array(self.Y_by_time)

    def simulate_till_next_extinction(self):
        while True:
            died = self.step()
            if died is not None:
                new_rs = set(self.reactant_species.tolist())
                new_rs.remove(died)
                self.reactant_species = array(list(new_rs))

                #save extinction event
                self.extinction_series.append(died)
                self.extinction_times.append(self.t[-1])

                #print "species",died,"died"
                break

    def simulate(self):

        while len(self.reactant_species)>1:
            self.simulate_till_next_extinction()

        self.extinction_series.append(self.reactant_species[0])
        self.extinction_times.append(self.t[-1])

    def simulate_till_t(self,tmax):
        while self.t[-1]<tmax:
            self.step()
            ndx = nonzero(self.current_N<=0)[0]
            if len(ndx)>0:
                self.current_N[ndx] = 0
                self.N[-1][ndx] = 0
                print len(ndx),"species died"
                break

    def get_extinction_events(self):

        return array(self.extinction_times), array(self.extinction_series)

if __name__=="__main__":
    import pylab as pl
    import seaborn as sns
    import time
    from scipy.optimize import curve_fit

    time_measurement = False

    Nsp = 3
    NWi = 2
    C = zeros((Nsp,NWi))
    C[0,0] = 1.
    C[1,1] = 1.
    C[2,0] = 1.
    C[2,1] = 1.
    y0 = 1.
    alpha = 10
    time_unit = '$T$'


    if time_measurement:
        measurements = 8

        Nmaxs = 9 * 2**arange(4,dtype=int)
        means = zeros(len(Nmaxs))
        stds = zeros(len(Nmaxs))
        result = zeros(measurements)
        for iN in range(len(Nmaxs)):
            N = Nmaxs[iN]
            print N
            for meas in range(measurements):
                print " ", meas
                start = time.time()
                sim = fluctuating_fitness_SSA(C,y0,alpha,N,method="exact",save_XY=True)                
                sim.simulate()
                end = time.time()
                result[meas] = end-start
            means[iN] = mean(result)
            stds[iN] = std(result)

        fig,ax = pl.subplots(1,2)
        ax[0].errorbar(Nmaxs,means,stds/sqrt(measurements))
        ax[0].set_xscale("log")
        ax[0].set_yscale("log")


        func = lambda x,a,b: a*x**b
        popt, pcov = curve_fit(func, Nmaxs, means)
        print popt
        ax[0].plot(Nmaxs,func(Nmaxs,*popt))

        x = log(Nmaxs)
        y = log(means)


        p = polyfit(x,y,1)

        print exp(p[1]), p[0]
        
        f = lambda x,p: exp(p[1])*x**p[0]
        ax[0].plot(Nmaxs,f(Nmaxs,p))
        ax[1].plot(x,y)
        ax[1].plot(log(Nmaxs),log(func(Nmaxs,*popt)))
        ax[1].plot(x,polyval(p,x))


        pl.show()


    else:
        Nmax = 60
        #sim = fluctuating_fitness_SSA(C,y0,alpha,Nmax,method="exact",save_XY=True,pl=pl)

        N_measurements = 10

        all_X = [ [] for sp in range(Nsp) ]

        for meas in xrange(N_measurements):
            sim = fluctuating_fitness_SSA(C,y0,alpha,Nmax,method="exact",save_XY=True)
            sim.simulate()
            #for i in range(tmax):
            #    sim.step()
            extinct_t, extinct_sp = sim.get_extinction_events()
            print extinct_t, extinct_sp

            t = sim.get_t()
            n = sim.get_N()
            X,Y = sim.get_XY()
            taus = sim.get_taus_till_now()
            fig,ax = pl.subplots(2,2)
            ax = ax.flatten()
            for sp in range(Nsp):
                print "species", sp
                print "Y", mean(Y,axis=0)[sp], std(Y,axis=0)[sp]
                print "X", mean(X,axis=0)[sp], std(X,axis=0)[sp]
                all_X[sp].extend(X[:,sp])


        figh, axh = pl.subplots(1,1)
        x = linspace(-3,3,100)
        for sp in range(Nsp):
            axh.hist(all_X[sp],alpha=0.2,normed=True,bins=40)
        axh.plot(x,mlab.normpdf(x,0.,1.))

        for sp in range(Nsp):
            ax[0].plot(t,n[:,sp],'-')
            ax[2].plot(t,Y[:,sp],'-')
            ax[3].plot(t,X[:,sp],'-')
        ax[0].plot(t,n.sum(axis=1),'-',lw=2)
        ax[0].set_xlabel(r'$t/$'+time_unit)
        ax[2].set_xlabel(r'$t/$'+time_unit)
        ax[1].set_xlabel(r'$\tau/$'+time_unit)
        ax[1].hist(taus,normed=True)

    pl.show()

    
